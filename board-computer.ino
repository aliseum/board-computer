#include <Wire.h>
#include <SPI.h>
#include <Servo.h>
#include <Timer.h>
#include <Adafruit_LSM9DS0.h>
#include <Adafruit_Sensor.h>  // not used in this demo but required!

Adafruit_LSM9DS0 lsm = Adafruit_LSM9DS0();

Servo servo;

Timer t;

int MACHINE_STATE = 0;

bool DataOut[10];

long int magnetometer_0[3];
long int magnetometer[3];
long int accelerometer_0[3];
long int accelerometer[3];

int ledEvent;

void destroy_loop() {
  servo.write(90);
  while (1);
}

void setupSensor()
{
  // 1.) Set the accelerometer range
  lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_4G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_6G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_8G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_16G);
  
  // 2.) Set the magnetometer sensitivity
  lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_4GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_8GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_12GAUSS);

  // 3.) Setup the gyroscope
  lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_500DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_2000DPS);
}

void readSensor(long int* mag, long int* acc)
{
  lsm.read();
  // ACCEL X Y Z MAG X Y Z GYRO X Y Z TEMP
  /*if (DataOut[0]) Serial.print((int)lsm.accelData.x); Serial.print(",");
  if (DataOut[1]) Serial.print((int)lsm.accelData.y); Serial.print(",");
  if (DataOut[2]) Serial.print((int)lsm.accelData.z); Serial.print(",");
  if (DataOut[3]) Serial.print((int)lsm.magData.x); Serial.print(",");
  if (DataOut[4]) Serial.print((int)lsm.magData.y); Serial.print(",");
  if (DataOut[5]) Serial.print((int)lsm.magData.z); Serial.print(",");
  if (DataOut[6]) Serial.print((int)lsm.gyroData.x); Serial.print(",");
  if (DataOut[7]) Serial.print((int)lsm.gyroData.y); Serial.print(",");
  if (DataOut[8]) Serial.print((int)lsm.gyroData.z); Serial.print(",");
  if (DataOut[9]) Serial.print((int)lsm.temperature); Serial.print(",");*/
  mag[0] = (long int)lsm.accelData.x;
  mag[1] = (long int)lsm.accelData.y;
  mag[2] = (long int)lsm.accelData.z;
  acc[0] = (long int)lsm.accelData.x;
  acc[1] = (long int)lsm.accelData.y;
  acc[2] = (long int)lsm.accelData.z;
}

double cos_theta(long int* v1, long int* v2) {
  double mod1 = v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2];
  double mod2 = v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2];
  double num = v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
  return num/(mod1*mod2);
}

inline long int norm2(long int* v) {
  return v[0] * v[0] + v[1] * v[1] + v[2] * v[2]; 
}

void setup() {
  for (int i = 0; i < 10; i++)
    DataOut[i] = true;
/*#ifndef ESP8266
  while (!Serial);     // attesa nel caso si utilizzi una scheda Arduino il cui Seriale non è immediatamente disponibile
#endif
  Serial.begin(9600);*/
  if (!lsm.begin())
  {
    t.oscillate(LED_BUILTIN, 20, HIGH);
    while (1);
  }
  //Serial.println("");
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  servo.attach(5);
  servo.write(30);
  delay(200);
  servo.write(0);
  delay(2000);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  readSensor(magnetometer_0, accelerometer_0);
  digitalWrite(LED_BUILTIN, LOW);
}

void loop() {
  readSensor(magnetometer, accelerometer);
  switch (MACHINE_STATE) {
    case 0: // In attesa di Lancio
      if (norm2(accelerometer_0)*4 < norm2(accelerometer)) { // Lancio...
        ledEvent = t.oscillate(LED_BUILTIN, 50, HIGH);
        int afterEvent = t.after(5000, destroy_loop, 0);
        MACHINE_STATE = 1;
      }
    break;
    case 1: // In Volo
      if (cos_theta(magnetometer, magnetometer_0) < 0) {
        digitalWrite(LED_BUILTIN, HIGH);
        servo.write(90);
        t.stop(ledEvent);
        MACHINE_STATE = 2;
      }
    break;
    case 2: // Post-Sgancio recupero
      while (1);
    break;
  }
  t.update();
  //delay(100);
}


